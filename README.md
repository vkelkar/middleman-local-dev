### [middlemanapp](https://middlemanapp.com) local-dev environment setup on a mac laptop


#### Resources and links

- [GitLab.com development using local Ruby/Node](https://gitlab.com/gitlab-com/www-gitlab-com/-/blob/master/doc/development.md)
- [GitLab.com using docker](https://about.gitlab.com/handbook/marketing/product-marketing/getting-started/102/)
- [Middlemanapp](https://middlemanapp.com) information/link

##### Prerequisites
- [Homebrew](https://brew.sh/) installed on macosx
- [Generate and Setup SSH keys](https://docs.gitlab.com/ee/gitlab-basics/create-your-ssh-keys.html) to clone/copy your codebase to local machine

- Get the code
  1. load your SSH keys into you SSH-agent
     ```
     ssh-add ~/.ssh/name-of-your-ssh-file
     ```
  1. confirm you have loaded your SSH key
     ```
     ssh-add -l
     ```
  1. Change to a location where you will put codebase
     ```
     cd ~/workspace or
     cd /Users/vkelkar/workspace
     ```
  1. Clone or download your codebase
     ```
     git clone git@gitlab.com:gitlab-com/www-gitlab-com.git
     ```
- Get Packages - Let's install packages needed to run codebase
  <!-- nvm, yarn, rbenv, python, pyenv packages -->
  ```bash
    brew install nvm yarn rbenv python pyenv
  ```
- Configure local environment
  - Configure nvm
    ```
    mkdir ~/.nvm

    ### add the following to your ~/.zshrc or ~/.bashrc files

    export NVM_DIR="$HOME/.nvm"
    [ -s "/usr/local/opt/nvm/nvm.sh" ] && . "/usr/local/opt/nvm/nvm.sh"  # This loads nvm
    [ -s "/usr/local/opt/nvm/etc/bash_completion.d/nvm" ] && . "/usr/local/opt/nvm/etc/bash_completion.d/nvm"  # This loads nvm bash_completion

    ```
  - Configure rbenv
    ```
    ### add the following to your ~/.zshrc or ~/.bashrc files

    export RUBY_CONFIGURE_OPTS="--with-openssl-dir=$(brew --prefix openssl@1.1)"

    ### for ruby environments (rbenv)

    eval "$(rbenv init -)"
    ```
  - Configure python
    ```
    ### add the following to your ~/.zshrc or ~/.bashrc files

    eval "$(pyenv init -)"
    ```
  - Configure middlemanapp
    ```
    ### add the following to your ~/.zshrc or ~/.bashrc files

    export ENABLE_LIVERELOAD=1
    ```
- Configure `${GIT_WORKING_DIRECTORY}`: it is a directory where your code resides
  ```bash
     example: /Users/vkelkar/workspace/${GIT_WORKING_DIRECTORY}
     example: /Users/vkelkar/workspace/www-gitlab-com
  ```
  Confirm you are pointing to correct git Repository
  ```bash
  cd ${GIT_WORKING_DIRECTORY}
  git remote -v
  ```
  ```
  origin	git@gitlab.com:gitlab-com/www-gitlab-com.git (fetch)
  origin	git@gitlab.com:gitlab-com/www-gitlab-com.git (push)
  ```
- Configure local runtime    
  1. source ~/.zshrc or ~/.bashrc
  1. cd ${GIT_WORKING_DIRECTORY}
  1. rbenv install
  1. rbenv local 2.6.5 # cat ${GIT_WORKING_DIRECTORY}/.ruby-version
  1. gem install bundler
  1. pyenv install 3.8.2
  1. pyenv global 3.8.2
  

- Troubleshooting 
  1. nvm install 12.4.0   # to install node
  1. bundle install       # to install missing gems

##### Build website locally
- See all possible build options
  ```bash
  ### assuming you are in
  ## /Users/vkelkar/workspace/${GIT_WORKING_DIRECTORY}
  ## /Users/vkelkar/workspace/www-gitlab-com

  bundle exec rake -T
  ```
- Build everything
  ```bash
  bundle exec rake build:all  # Build the entire site, including all sub-sites
  ```
- Build only handbook
  ```bash
  bundle exec rake build:handbook  # Build sites/handbook site
  ```
##### Run website locally
- Start the local website
  ```bash
  ### assuming you are in
  ## /Users/vkelkar/workspace/${GIT_WORKING_DIRECTORY}
  ## /Users/vkelkar/workspace/www-gitlab-com
  bundle exec middleman
  ```
  It will take 2-5min for the local application to started
  ```bash
  ### In a browser, try the following URL

  http://localhost:4567/
  ```

###
# update items 
###

### What's next?

[Local workflow to create Merge Request (MR)](https://gitlab.com/vkelkar/middleman-local-dev/-/blob/master/local-workflow.md)

