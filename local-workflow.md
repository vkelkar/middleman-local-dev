## Local workflow to generate a Merge Request (MR)

#### Tools needed
1. Editor of choice (Example: ViM, Emacs, VSCode, Atom, SublimeText, Notepad++, ...etc)
1. Command line (Example Mac: terminal or iterm2, Linux: Gnome terminal or Tilix, ...etc)
1. [**optional**] GitLab CLI (example: [`lab`](https://github.com/zaquestion/lab) cli)

### Background and Concepts

* Before we describe the local workflow, it would be beneficial to read through [an introduction to Merge Requests (**MR**)](https://docs.gitlab.com/ee/user/project/merge_requests/getting_started.html)

* [Best practices](https://docs.gitlab.com/ee/user/project/merge_requests/getting_started.html#recommendations-and-best-practices-for-merge-requests) for MR is also a good read before we dive into local workflows.

### **MR** Approache(s)

1. The easiest approach to MR is to perform the work using GitLab's **built-in** IDE. If your changes/commits are relatively small, you can use the WebIDE to make changes/create commit, create the branch and start a MR for review.

1. Another approach would involve creating a feature branch on the GitLab server first using web interface. This will allow you to pull down the feature branch locally and work on your commits locally. Then using web interface you can create a MR. This approach will automatically delete the branch on server once MR is accepted.

    - Don't forget the best practices for MR:
      1. Title for MR - start with `Draft:` (Should be done for you already)  
      1. Merge options
          - **Check** delete source branch after merge (should be checked for you)
          - **Check** squash commits when MR is accepted (As a best-practice, you should consider checking it if you have multiple commits in your feature-branch)    


1. You can also start the feature branch locally and then push the branch to the server for creating an MR. In this approach, you have to be comfortable managing the branches yourself. This approach will require a manual step of deleting the local feature-branch after MR has been accepted.

### Local workflow

For the local workflow, I will start with the third approach first. 
  - Local editor: \
    I usually switch between `atom` and `vim` editors.
  - Git client: \
    I usually run git commands using `Tilix` on Linux or `iterm2` on Mac.


| | Command description | CLI Command | Output /  Screenshot |
|-|----- | ----- | ----- |
|1| From Master/main branch, create a local feature-branch | git checkout -b local-workflow-create-branch-locally-first |  Output: `Switched to a new branch 'local-workflow-create-branch-locally-first'`  |
|2| Add files/content/commits to your local feature-branch | git add local-workflow.md && git commit -m 'Added table to capture commands'| Output ` 1 file changed, 11 insertions(+)`|
|3| After all commits, push your local feature-branch to server | git push origin local-workflow-create-branch-locally-first | Output ` * [new branch]      local-workflow-create-branch-locally-first -> local-workflow-create-branch-locally-first` |
|4| Using GitLab web interface create the MR| web-interface action | MR created beginning with DRAFT: && don't forget to squash commits if you have multiple commits in the local-branch |
